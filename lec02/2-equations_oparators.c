#include <stdio.h>
#include <math.h>
/**
 * main function - operator precedence
 * Return 0 - success
 */
 
int main(void)
{
	int a = 5;
	int x = 2;

	int equation = a * pow(2, 3) + 7;

	printf("The solution for equation is: %d\n", equation);

	return (0);
}

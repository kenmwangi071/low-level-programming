#include <stdio.h>

/*
 * main function - prints sentence with backlash sequence
 * 0: Return 0 (success)
*/

int main(void)
{
	char name;

	printf("Your name says \"Hello\" from \t\t \\ICEN360 %s\n", &name);
	

	return (0);
}
